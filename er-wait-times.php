<?php
/**
 * Plugin Name:     Er Wait Time Plugin
 * Description:     Queries custom Facility Codes to return ER Wait Times in Minutes in popup window. 
 * Text Domain:     er-wait-time-plugin
 * Version:         1.0.0
 *
 * @package         er-plugin
 */


require_once( 'er-wait-times/init.php' );
