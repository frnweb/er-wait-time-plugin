const path = require('path');

module.exports = {
  entry: './src/react/index.js',
  output: {
    filename: 'react.js',
    path: path.resolve(__dirname, 'build'),
  },
  watch: true,
  mode: 'development',
  module: {
    rules: [
        {
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: {
                loader: "babel-loader",
                options: {
                presets: ["@babel/preset-react", "@babel/preset-env"]
                }
            }
        },
        {
            test: /\.s[ac]ss$/i,
            use: [
              {
                loader: 'style-loader',
              },
              {
                loader: 'css-loader',
                options: {
                    sourceMap: true
                }
              },
              {
                loader: 'resolve-url-loader',
                options: {
                    sourceMap: true
                }
              },
              {
                loader: 'sass-loader',
                options: {
                    sourceMap: true
                }
              },
            ],
          },
        {
          test: /\.(png|svg|jpg|jpeg|gif)$/i,
          use: [
            {
              loader: 'url-loader',
              options: {
                limit: 8192,
                name: 'static/media/[name].[hash:8].[ext]'
              },
            },
          ],
        }
    ]
    }
};