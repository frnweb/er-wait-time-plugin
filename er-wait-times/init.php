<?php 

// include the Composer autoload file
require 'vendor/autoload.php';

/**
 * Register ACF Theme Options Page.
 */
add_action( 'wp_loaded','my_load_popup' );
function my_load_popup() {


	if( function_exists('acf_add_options_page') ) {
		acf_add_options_page(array(
			'page_title' 	=> 'ER Wait Time Settings',
			'menu_title'	=> 'ER Wait Time Settings',
			'menu_slug' 	=> 'er-wait-settings',
			'capability'	=> 'edit_posts',
			'redirect'		=> false,
			'post_id' => 'er_fields',
		));	
	}

	require_once(plugin_dir_path( __FILE__ ) . "/app/fields/partials/admin.php");
	
	acf_add_local_field_group($optionsER->build());

	$er_fields = get_fields('er_fields');

	$er_codes = [];

	if ($er_fields['facility_list']['facility_repeater']) {

		$repeater = $er_fields['facility_list']['facility_repeater'];
		$new_array = array();
		foreach ($repeater as $to_obj)
		{
			$new_array[] = (object)$to_obj;
		}
	
		foreach ($repeater as $fields) {
			if ($fields['facility_code'] !== ''){
				array_push($er_codes, $fields['facility_code']);
			}
		} 
	
		$list = implode(',', $er_codes);

		if ($list !== ''){
			$erurl = 'https://api.uhsinc.com/erwaittime/api/waittime/?faccodes='.$list;

			$opts = array("http" =>
			array("header" => 'Authorization: Bearer LH6d4kj2L7alooOCw4w65UXuZ5rtZFIdZPaYe6Cw6ybBqL-2HzDc14KD56qyUvy9JzDK_Kb9BK1EPS7JkhassoEEolwf4tMOvjVOn_vSFlTwIk6_rheYVPMmfa0ek1CvNiu47ShmdKocxn4LMcgjZ2COh_9nls28ceWHPvdV4P77avb2jfph-3R1HB7vIUKt1CXLQZIpg4JpYivCyVgHEmnfuFxANJwPqvsQIG6DSOVQdTC9AGYXYqy-uKVIuKrLE3UjDVAKK--1Wz3rAHH34gD-yM8')
			);
	
			$ercontext = stream_context_create($opts);
		
			$response = file_get_contents($erurl, false, $ercontext);

			$arrayTemp = array();
	
			foreach ($new_array as $field1) {
				foreach (json_decode($response) as $field2) {
					if ($field1->facility_code === $field2->facilityCode) {
						$arrayTemp[] = (object) array_merge((array) $field1, (array) $field2);
					}
				}
			}
			
			$results = json_encode($arrayTemp);

			if (! is_admin() && $GLOBALS['pagenow'] !== 'wp-login.php' ){
				wp_enqueue_script( 'wm_react', plugin_dir_url( __FILE__ ) . '/build/react.js', array( 'jquery' ), null, true );
				echo "<script> const data = $results </script>";
			}
		}
		
	};
}